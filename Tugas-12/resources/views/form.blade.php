<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sing Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="Fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="Lname"><br><br>
        
        <label>Gender:</label><br><br>
        <input type="radio">Male<br>
        <input type="radio">Female<br>
        <input type="radio">Other<br><br>
        
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Australia">Australia</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="20" rows="10"></textarea><br>

        <button type="submit">Sing Up</button>
    </form>
    
</body>
</html>