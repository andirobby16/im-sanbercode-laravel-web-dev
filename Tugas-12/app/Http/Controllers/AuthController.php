<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }
    public function welcome(Request $request){
       $namaDepan = $request['Fname'];
       $namaBelakang = $request['Lname'];

       return view('welcome', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
       
    }
}
