@extends('layouts.master')
@section('title')
    Halaman Detail Cast
@endsection

@section('subtitle')
    Detail Cast
@endsection

@section('content')
    <h1>Nama : {{$cast->nama}}</h1>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>

@endsection