<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$animal = new Animal("shaun");
echo "Nama Hewan \t: $animal->name <br>";
echo "Jumlah kaki \t: $animal->legs <br>";
echo "Darah dingin \t: $animal->cold_blooded <br><br>";

$kodok = new Frog("buduk");
echo "Nama Hewan \t: $kodok->name <br>";
echo "Jumlah kaki \t: $kodok->legs <br>";
echo "Darah dingin \t: $kodok->cold_blooded <br>";
echo "Jump : ". $kodok->jump()."<br><br>";

$kera = new Ape("kera sakti");
echo "Nama Hewan \t: $kera->name <br>";
echo "Jumlah kaki \t: $kera->legs <br>";
echo "Darah dingin \t: $kera->cold_blooded <br>";
echo "Yell : ". $kera->yell();
?>